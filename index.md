---
layout: default
template: default
title: StreetSign Project overview
---

# StreetSign

StreetSign is a digital signage system, originally written for the [TeenStreet 2013 (Germany)](http://teenstreet.de) youth congress.  It works with a single server, and multiple client computers (for TS2013 we used Raspberry Pis) which connect over the network.  It is light-weight enough that a raspberry pi can run as the server as well, for smaller installations.

For best performance, currently, something with a bit more oomph than a Raspberry Pi is recommended, although if you stick to simple themes, low animation, and basic annoucements, then a pi is fine.

It's Free Software, released under the GPL3 licence, so you can download the source code, and tinker and hack it to suit your own purposes as much as you need.

## Status

Alpha/Testing.  We used it quite successfully at Teenstreet, and it's been in heavy development since then, with most of the problems noticed in that event either fixed or minimized.  It's still in heavy development, and it's not ready for installation and running by non-techies (which is the ultimate aim).  Once it's running, it is quite usable by non-technical people, but it's recommended to have some kind of system administrator around to help.

## Features

<div style="float: right">
<a href="http://www.w3.org/html/logo/">
<img src="http://www.w3.org/html/logo/badge/html5-badge-h-solo.png" width="63" height="64" alt="HTML5 Powered" title="HTML5 Powered">
</a></div>

- Fast HTML5 "screen output" displays
- Multi-user access, with groups and permissions for each feed
- Simple separation of content and design
- RSS "external data importing".
- Single file databases for simplified administration
- Built in basic web server for basic installations
- Easily expandable with your own plugins, post types, data importers, etc.
- Basic included post types: HTML (rich text), images, plain text, external web pages

### Easy to use web based administration including:
- Point and Click Screen layout and theme design (No XML or other complexities)
- Multiple users, groups and user access control/permissions
- Quite a bit more.

[![Screen Designer](img/thumbnails/screen_designer.png "Screen Designer")](img/screen_designer.png)
[![Feeds Overview](img/thumbnails/feeds_overview.png "Overview of feeds")](img/feeds_overview.png)


## So where are we at now?

This project page is only just starting now, there is much more complete documentation at:

[streetsign.readthedocs.org](http://streetsign.readthedocs.org/en/latest/)

and the main server repository is at:

[bitbucket.org/dfairhead/streetsign-server](https://bitbucket.org/dfairhead/streetsign-server)

and a basic full screen raspberry pi web kiosk installation script (ansible) is at:

[bitbucket.org/dfairhead/raspberry-kiosk](https://bitbucket.org/dfairhead/raspberry-kiosk)

There is plenty more 'parts' of the project which will work their way online over the next few months, such as the "sneakersync" scripts we used to syncronise multiple pi's across the campus when the network went down for a few days (each one ran it's own streetsign installation, and automatically pulled data from a USB stick when it was plugged in, so we had one master install locally, and then ever
y few hours bicycled around, plugged in the stick, and kept everything up to date).

I'm also experimenting with hardware accelerated web browsers for better scrolling, etc, possibly using 
firefoxOS, which I think is very worth doing.

Mostly it's just been one developer (me), but I'm very interested in help, as I think this does have a lot of potential to be very useful.  Get in touch!
