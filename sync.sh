#!/bin/sh

s3cmd sync --exclude '*.git*' --exclude '*.swp' _site/ s3://www.streetsign.org.uk
